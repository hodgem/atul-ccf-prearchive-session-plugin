## CCF Pre Archive Session plugin ##

To build the plugin, run the following command from within the plugin folder:

```bash

./gradlew fatjar
```


On Windows, you may need to run:

```bash

gradlew fatjar
```


If you haven't previously run this build, it may take a while for all of the dependencies to download.

You can verify your completed build by looking in the folder **build/libs**. It should contain a file named something like **ccf-prearchive-cleanup-plugin-all-0.1.0.jar**. This is the plugin jar that you can install in your XNAT's **plugins** folder.
